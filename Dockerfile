FROM wordpress:latest
ARG XDEBUG_PATH="/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"
RUN apt-get update
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && echo "xdebug.mode = debug" >> $XDEBUG_PATH \
    && echo "xdebug.client_port = 9003" >> $XDEBUG_PATH \
    && echo "xdebug.client_host = 'host.docker.internal'" >> $XDEBUG_PATH \
    && echo "xdebug.start_with_request=yes" >> $XDEBUG_PATH
RUN service apache2 restart