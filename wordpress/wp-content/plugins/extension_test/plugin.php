<?php
/*
Plugin Name: Extension_Test
Description: ..........
Author: Oihan Ithurritze
Version: 0.1
*/

add_action( 'admin_menu', 'add_php_page' );

function add_php_page() {
    add_submenu_page(
        'plugins.php',
        'Extension_Test Exec',
        'Extension_Test Debug',
        'manage_options',
        'php-info-page',
        'debugged_function');
}

function debugged_function() {
    echo "Ligne 1<br />";
    echo "Ligne 2<br />";
}

?>